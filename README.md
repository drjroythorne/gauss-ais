# gauss-ais

Experiments using AIS data to infer ocean surface currents with multiple-output Gaussian process regression.

# Data

[Marine Cadastre](https://marinecadastre.gov/ais/)

Download data for first quarter of 2017. UTM Zone 11 (includes port of LA - busines container port in USA).

# Story

- search for raw data (MarineTracker, MarineCadastre)
- lat-lon to UTM (pyproj vs. utm) (vectorised)
- filter to dense traffic region (prototype on laptop + more likely that this works where we have multiple measurements)
- TUGS (filter them out)
- vesselfinder.com (scrape)
