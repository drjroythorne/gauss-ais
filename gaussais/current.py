from functools import partial
import numpy as np

def _current_field(x,y, omega=1.0, alpha=1.0, magnitude=1, x_scale=1, y_scale=1):
    '''
    :param np.array x:
    :param np.array y:
    :param omega: Length scale for oscillations.
    :param alpha: Length scale for polynomial terms.
    :param magnitude: Magnitudey
    '''

    return (
        magnitude*(np.cos(2*np.pi*omega*x/x_scale) - np.sin(2*np.pi*omega*y/y_scale)),
        magnitude*(0.1 - (0.01*alpha*x/x_scale)**3 + np.sin(2*np.pi*omega*y/y_scale) - (0.01*alpha*y/y_scale)**2)
    )

CURRENT_2_4 = partial(_current_field, omega=2, alpha=1, magnitude=0.1)