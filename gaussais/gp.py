import tensorflow as tf
import numpy as np
from gpflow.utilities import print_summary
import gpflow
from gpflow.ci_utils import ci_niter

import matplotlib
import matplotlib.pyplot as plt

gpflow.config.set_default_float(np.float64)
gpflow.config.set_default_summary_fmt("notebook")
np.random.seed(0)
get_ipython().run_line_magic('matplotlib', 'inline')

np.random.seed(1)  # for reproducibility


class HeteroskedasticGaussian(gpflow.likelihoods.Likelihood):
    def log_prob(self, F, Y):
        raise NotImplementedError

    def conditional_mean(self, F):
        raise NotImplementedError

    def conditional_variance(self, F):
        raise NotImplementedError

    def variational_expectations(self, Fmu, Fvar, Y):
        coordinates, cov_rows = Y[:, 0:1], Y[:, 1:3]
        y_positions = tf.transpose(tf.reshape(coordinates,(2,-1,1)), perm=(1,0,2))
        meas_cov = tf.transpose(tf.reshape(cov_rows,(2,-1,2)),perm=(1,0,2))

        Fmu_positions = tf.transpose(tf.reshape(Fmu,(2,-1)))

        num_cov = y_positions.shape[0]
        F_cov = tf.transpose(tf.linalg.diag_part(
            tf.transpose(
                tf.reshape(
                    Fvar,
                    (2,num_cov,2,num_cov)
                )
                , perm=(0,2,1,3)
            )
        ))
        # Calculate the log normal density for this 2*2 block-diagonal covariance matrix. 
        dy = y_positions - tf.reshape(Fmu_positions, (-1,2,1))
        c = tf.linalg.cholesky(meas_cov)
        log_det_meas_cov = 2.0*tf.reduce_sum(tf.math.log(tf.linalg.diag_part(c)))
        q = tf.linalg.triangular_solve(c, dy)
        m = tf.linalg.triangular_solve(c, F_cov)
        m_prime = tf.linalg.triangular_solve(tf.transpose(c, perm=(0,2,1)), m, lower=False)
        tr_inv_meas_cov_F_cov = tf.reduce_sum(tf.linalg.trace(m_prime))
        return -0.5*(2.0 * np.log(2 * np.pi) + log_det_meas_cov + tr_inv_meas_cov_F_cov + tf.reduce_sum(tf.math.square(q)))


class ReplicatedKernel(gpflow.kernels.Kernel):
    def __init__(self, variance=1.0, lengthscale=1.0, **kwargs):
        """
        :param variance: the (initial) value for the variance parameter
        :param lengthscale: the (initial) value for the lengthscale parameter(s),
            to induce ARD behaviour this must be initialised as an array the same
            length as the the number of active dimensions e.g. [1., 1., 1.]
        :param kwargs: accepts `name` and `active_dims`, which is a list of
            length input_dim which controls which columns of X are used
        """
        for kwarg in kwargs:
            if kwarg not in {'name', 'active_dims'}:
                raise TypeError('Unknown keyword argument:', kwarg)

        super().__init__(**kwargs)
        self._kernel = gpflow.kernels.SquaredExponential(variance=variance, lengthscale=lengthscale)
    
    def K(self, X, X2=None, presliced=False):

        N_X = int(X.shape[0]/2)
        N_X2 = N_X

        X2_0, X2_1 = None, None
        if X2 is not None:
            N_X2 = int(X2.shape[0]/2)
            X2_0 = X2[:N_X2,:-1]
            X2_1 = X2[N_X2:,:-1]

        K_upp = self._kernel(X[:N_X,:-1], Y=X2_0)
        K_low = self._kernel(X[N_X:,:-1], Y=X2_1)
        return tf.concat([
            tf.concat([K_upp, tf.zeros((N_X,N_X2), dtype=gpflow.config.default_float())], axis=-1),
            tf.concat([tf.zeros((N_X,N_X2), dtype=gpflow.config.default_float()),K_low], axis=-1)
        ], axis=0)

    def K_diag(self, X, presliced=False):
        return self._kernel.K_diag(X, presliced)


# model construction (notice that num_latent is 1)
#likelihood = HeteroskedasticGaussian()
#model = gpflow.models.VGP((X, Y_data), kernel=ReplicatedKernel(lengthscale=1.0), likelihood=likelihood, num_latent=1)


@tf.function
def objective_closure():
    return - model.log_marginal_likelihood()

#opt = gpflow.optimizers.Scipy()
#opt.minimize(objective_closure,
#             model.trainable_variables,
#             options=dict(maxiter=ci_niter(1000)))


# Prediction
#xx = np.c_[np.linspace(-5, 5, 200)[:, None], np.zeros(200)]
#mu, var = model.predict_f(xx)

# Cholesky decomp for the inverses

def make_Y(mu_current, sigma_current):
    Y_mu_sigma = tf.reshape(tf.transpose(sigma_current, perm=(2,0,1)), (-1,2))
    mu_current_flattened = tf.reshape(tf.transpose(mu_current),(-1,1))
    return tf.concat([mu_current_flattened, Y_mu_sigma], axis=-1)

def make_X(position):
    flattened_position = tf.reshape(tf.transpose(position),(-1,))
    return tf.stack([flattened_position, np.concatenate([np.zeros(position.shape[0]), np.ones(position.shape[0])])], axis=-1)


#Test it
#tf.matmul(tf.matmul(tf.transpose(y), tf.linalg.inv(x)),y)

