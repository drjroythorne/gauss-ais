from collections import namedtuple
import itertools
import logging
import math
import numpy as np

from .course_correction import heading_for_cog

class EulerMaruyamaGenerator:
    def __init__(self, mu, sigma, tau, dt):
        self._mu = mu
        self._sigma = sigma
        self._tau = tau
        self._dt = dt
        self._sqrtdt = np.sqrt(self._dt)
        self._sigma_bis = self._sigma * np.sqrt(2. / self._tau)
    
    def yield_series(self, start):
        yield start
        x = start
        new_x = None
        while True:
            if new_x is not None:
                x = new_x
                self._mu = new_x

            x += self._dt*(self._mu-x)/self._tau + \
                self._sigma_bis*self._sqrtdt*np.random.randn()
            new_x = yield x


TrajectoryElement = namedtuple('TrajectoryElement', ['t', 'x', 'v', 'stw', 'heading'])


class VesselTrajectoryGenerator:
    
    _MAX_SOG = 25

    def __init__(
        self,
        bounding_box,
        current_field,
        mu_mu_stw,
        sigma_mu_stw,
        sigma_stw,
        sigma_theta,
        course_correction_period,
        tau,
        dt,
        trajectory_termination_threshold
    ):
        """
        :param np.array bounding_box: 2x2 array with first row bottom-left, second row top-right
        :param callable current_field:
        :param float mu_mu_stw: Mean of mean vessel speeds through water [ms^-1].
        :param float sigma_mu_stw: Std dev of mean vessel speeds through water [ms^-1].
        :param float sigma_stw: Speed through water noise [ms^-1].
        :param float sigma_theta: Heading noise [rad].
        :param float course_correction_period: [s].
        :param float tau: Time constant (common for sog and theta).
        :param float dt: Time step.
        :param float trajectory_termination_threshold:
        """
        self._sides = self._make_sides(bounding_box)
        self._current_field = current_field
        self._mu_mu_stw = mu_mu_stw
        self._sigma_mu_stw = sigma_mu_stw
        self._sigma_stw = sigma_stw
        self._sigma_theta = sigma_theta
        self._course_correction_period = course_correction_period
        self._tau = tau
        self._dt = dt
        self._trajectory_termination_threshold = trajectory_termination_threshold
        self._logger = logging.getLogger(__name__) 

    def __call__(self):    
        entry_side_index, exit_side_index = self._get_entry_exit_side_indices()
        entry_point = self._get_point_on_side(side_index = entry_side_index)
        exit_point = self._get_point_on_side(side_index = exit_side_index)
        self._logger.info(entry_point, exit_point)
        stw_generator = self._yield_stw()
        
        desired_cog_vector = exit_point - entry_point
        initial_heading = np.math.atan2(desired_cog_vector[1], desired_cog_vector[0])
        theta_generator = self._yield_theta(initial_heading)

        iterations_per_course_correciton = math.floor(self._course_correction_period / self._dt)

        x = entry_point
        for i, (stw, theta) in enumerate(zip(stw_generator, theta_generator)):
            self._logger.info('theta: %s', str(theta))
            c = self._current_field(*x)

            # Do we need to modify our course?numpy list of arrays to two arrays
            if i%iterations_per_course_correciton == 0:
                v_target = exit_point - x
                self._logger.info('v_target: %s', v_target)
                heading = heading_for_cog(
                    cog=np.math.atan2(v_target[1], v_target[0]),
                    current = c,
                    stw = stw
                )
                self._logger.info('New heading %s at time %s', str(heading), str(i*self._dt))
                print()
                theta_generator.send(heading)

            v = c + stw*np.array([np.cos(theta), np.sin(theta)])
            self._logger.info('v: %s', v)
            x += self._dt * v
            self._logger.info('x: %s', x)
            dist_to_exit = np.linalg.norm(x-exit_point)
            self._logger.info('Distance to exit: %s', str(dist_to_exit))
            if dist_to_exit < self._trajectory_termination_threshold:
                return
            yield TrajectoryElement(
                t = i*self._dt,
                x = np.copy(x),
                v = v,
                stw = stw,
                heading = theta
            )
                

    @staticmethod
    def _make_sides(bounding_box):
        corners = [
            bounding_box[0,:],
            np.array([bounding_box[0,0],bounding_box[1,1]]),
            bounding_box[1,:],
            np.array([bounding_box[0,1],bounding_box[1,0]])
        ]

        return np.array([
            [corners[0], corners[1]],
            [corners[1], corners[2]],
            [corners[2], corners[3]],
            [corners[3], corners[0]]
        ])

    @staticmethod
    def _get_entry_exit_side_indices():
        return np.random.choice(np.arange(4), size=2, replace=False)
    
    def _get_point_on_side(self, side_index):
        lamb = np.random.uniform()
        return np.dot(self._sides[side_index], np.array([lamb, 1-lamb]))
    
    def _get_trajectory(self, origin, destination):
        pass

    def _yield_stw(self):
        mu = self._get_vessel_mu_stw(mu = self._mu_mu_stw, sigma = self._sigma_mu_stw)
        stw_generator = EulerMaruyamaGenerator(
            mu=mu,
            sigma=self._sigma_stw,
            tau=self._tau,
            dt=self._dt
        )
        yield from stw_generator.yield_series(
            start=np.random.normal(loc=mu, scale=self._sigma_stw)
        )
    
    def _yield_theta(self, initial_heading):
        theta_generator = EulerMaruyamaGenerator(
            mu = initial_heading,
            sigma = self._sigma_theta,
            tau = self._tau,
            dt = self._dt
        ).yield_series(start=initial_heading)
        
        for theta in theta_generator:
            new_heading = yield theta
            if new_heading is not None:
                theta_generator.send(new_heading)

    def _get_vessel_mu_stw(self, mu, sigma):
        return np.clip(
            np.random.normal(loc=mu, scale=sigma),
            a_min=0,
            a_max=self._MAX_SOG
        ) 