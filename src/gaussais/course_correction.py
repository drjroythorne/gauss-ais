import numpy as np

def heading_for_cog(cog, current, stw):
    """ Return the required heading for a given desired couse over ground (cog),
    current and speed through water (stw).

    :param float cog: Desired course over ground (-np.pi < cog <= np.pi).
    :param np.array current: 2d current vector.
    :param float stw: Speed through water (0 < stw).
    :return float:
    """
    
    cog_hat = np.array([np.cos(cog),np.sin(cog)],dtype=float)
    norm_current = np.linalg.norm(current)
    if norm_current <= np.finfo(float).tiny:
         return cog
    delta = np.arccos(np.dot(cog_hat,current)/norm_current)
    abs_correction_angle = np.arcsin((norm_current/stw)*np.sin(delta))
    sign_correction_angle = np.sign(np.cross(current, cog_hat))
    return cog+(sign_correction_angle*abs_correction_angle)