from hypothesis import given
import hypothesis.strategies as st
import hypothesis.extra.numpy as hnp
import numpy as np

from gaussais.course_correction import heading_for_cog

@given(
    cog = st.floats(-np.pi, np.pi, exclude_max=True),
    current = hnp.arrays(np.float, 2, elements=st.floats(-1,1)),
    stw = st.floats(5,10)
)
def test_heading_for_cog(cog, current, stw):
    heading = heading_for_cog(cog, current, stw)
    vtw = stw*np.array([np.cos(heading),np.sin(heading)])
    vog = vtw+current
    cog_derived = np.math.atan2(vog[1], vog[0])

    np.testing.assert_almost_equal(cog_derived, cog)
